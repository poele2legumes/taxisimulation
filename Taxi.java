/**
 * Un taxi est capable de transporter un unique passager.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class Taxi extends Vehicule
{
    private Passager passager;
    
    /**
     * Constructeur des objets de la classe Taxi
     * @param company La compagnie de taxi. Ne doit pas être null.
     * @param position La position de départ du véhicule. Ne doit pas être null.
     * @throws NullPointerException Si la compagnie ou la position est null.
     */
    public Taxi(TaxiCompany company, Position position)
    {
        super(company, position);
    }
    
    /**
     * Effectue les actions d'un taxi.
     */
    public void act()
    {
        Position destination = getDestinationPosition();
        if(destination != null) {
            // Trouve où aller ensuite.
            Position next = getPosition().nextPosition(destination);
            setPosition(next);
            if(next.equals(destination)) {
                if(passager != null) {
                    avertiPassagerArrivee(passager);
                    faitDescendrePassager();
                }
                else {
                    avertiPriseEnChargeArrivee();
                }
            }
        }
        else {
            incrementInactiviteCompteur();
        }
    }

    /**
     * Est-ce que le taxi est libre ?
     * @return Si oui ou non le taxi est libre.
     */
    public boolean estLibre()
    {
        // **** CODE A MODIFIER POUR LE DS ****
        Position destination = getDestinationPosition();
        
        if(passager == null && destination == null){
        	return true;
        } else {
        	return false;
        }
    	
    	 
    }
    
    /**
     * Reçoit une position de prise en charge.
     * Ceci devient la position de destination.
     * @param position La position de prise en charge.
     */
    public void setPriseEnChargePosition(Position position)
    {
        setDestinationPosition(position);
    }
    
    /**
     * Prend en charge un passager.
     * Enregistre sa destination comme la position de destination.
     * @param passager Le passager.
     */
    public void prendEnCharge(Passager passager)
    {
        this.passager = passager;
        setDestinationPosition(passager.getDestination());
    }

    /**
     * Fait descendre le passager.
     */
    public void faitDescendrePassager()
    {
        passager = null;
        effaceDestinationPosition();
    }
    
    /**
     * Retourne des détails du taxi, tels que sa position.
     * @return Une représentation du taxi sous forme chaîne de caractères.
     */
    public String toString()
    {
        return "Taxi à " + getPosition();
    }
}
