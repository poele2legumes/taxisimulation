# Projet: taxi-simulation

**Durée du devoir surveilé** : 4 heures

**Documentation** : supports de cours fournis sur Arche, TPs précédents, documentation Java Oracle (http://docs.oracle.com/javase/8/docs/api/index.html).

## Introduction

L'objectif de ce projet est de faire une simulation de compagnie de taxis,
dont voici l'expression du besoin :

> La compagnie met en oeuvre des taxis individuels. Les taxis sont
> utilisés pour transporter un individu (ou un petit groupe).
> Quand la compagnie reçoit un appel d'un individu, d'un hôtel ou d'un
> organisme touristique, il essaye de planifier un véhicule pour
> prendre en charge la course. Si il n'y a pas de véhicules libres,
> il n'est mis en oeuvre aucune forme de file d'attente.
> Quand un véhicule arrive à un lieu de prise en charge, le conducteur
> avertit la compagnie. De la même manière, quand un passager descend
> d'un taxi, le conducteur le signale à la compagnie.
>
> Il est souhaitable que le système garde des informations
> sur les demandes qui n'ont pu être satisfaites.
> Il fournit également des détails sur le temps passé par les
> véhicules dans chacune des activités suivantes : transport de passagers,
> se dirigeant vers un lieu de prise en charge et inactif.

Le logiciel de simulation a été commencé par un ingénieur,
qui a dû partir travailler sur un autre projet.
Vous avez été recruté pour continuer le travail de cet ingénieur,
vous devrez télécharger le programme réalisé par cet ingénieur et le compléter.


## Consignes

Vous avez le droit de consulter vos précédents programmes, la documentation
Java sur Internet, mais n'avez pas le droit de vous faire aider par des
personnes autre que votre enseignant, qu'elles soient dans la salle ou à distance.

Toute tentative de fraude sera sanctionnée par la note 0.

En cas de problèmes, si vous voulez récupérer un fichier enregistrez dans
un `commit` précédent utilisez la commande : `git checkout monfichier.java`

Chaque partie peut être réalisée en 1 heure environ.

Bon courage !

- - -

# Préparation du projet (1 point)

1. Créer une bifurcation (fork) du projet **taxi-simulation** dans votre compte Bitbucket.
2. Ajouter `lcpierron` en administrateur du projet.
3. Cloner votre projet sur votre machine locale.

La simulation peut être testée en créant un objet `Demo` et en exécutant
la méthode `run()`.

- - -

# Première partie : Compléter le code pour passer les tests (6 points)

Vous devez compléter les classes du programme pour passer tous les tests unitaires.
Afin de vous donner une ligne de conduite, voici les différentes
méthodes à écrire ou modifier :

Vous devez écrire les constructeurs d'objets des classes suivantes :

* Constructeur de la classe `Vehicule` : `Vehicule(TaxiCompany company, Position position)`,
   il doit positionner les variables de classe et contrôler les paramètres.

* Constructeur de la classe `Passager` : `Passager(Position prise_en_charge, Position destination)`,
   il doit positionner les variables de classe et contrôler les paramètres.

* Constructeur de la classe `Position` : `Position(int x, int y),
   il doit positionner les variables de classe et contrôler les paramètres.

Vous devez compléter les méthodes suivantes, lisez le texte **javadoc**
correspondant pour comprendre ce qu'elles doivent faire :

* Méthode `boolean estLibre()` de la classe `Taxi`, un taxi est libre si
   il n'a pas de passager et de destination en cours.

* Méthode `boolean demandeCourse()` de la classe `PassagerSource`, doit créer
  un nouveau passager et demander une course à la compagnie de taxi.

* Méthode `Position nextPosition(Position destination)` de la classe `Position`,
   qui doit retourner la position suivante d'un véhicule.
   Cette nouvelle position est définie par un déplacement de valeur absolue 1 sur l'axe des x
   ou des y, ce qui donne huit vecteurs de déplacement possibles : (-1,-1), (-1,0), (-1,1), (0,-1), (0,1), (1,-1) (1,0), (1,1). Vous devez choisir le vecteur qui rapproche le plus le véhicule de la destination donnée en paramètres.
   Si vous avez des difficultés à réaliser cette méthode vous pouvez passer à la suite
   du devoir.

 N'oubliez pas de *committer* votre travail après chaque modification avec un message
 explicite.

 À la fin de cette partie, même si vous n'avez pas tout fait et y reviendrez plus
 tard, synchronisez votre code sur Bitbucket : `git push --all`

- - -

# Seconde partie : Réécriture de code (6 points)

Dans cette seconde partie, il vous faudra ajouter des méthodes
et modifier un peu la structure du programme.

## Prix d'une course (1 point)

Ajoutez à `TaxiCompany` une méthode de calcul du prix d'une course
dont la signature sera :

```java
int getPrixDeCourse(Passager passager)
```

Le prix d'une course `prix` est défini par la formule :

`prix = tarif_de_prise_en_charge + tarif_kilometrique * longueur_course`

Vous devrez ajouter deux constantes à la classe `TaxiCompany` pour défnir
les deux valeurs tarifaires.

## Ajout d'un cas d'erreur (1 point)

Le constructeur de la classe `Passager` doit renvoyer une exception
quand la destination est égale à la position de prise en charge.
Choisissez une exception existante appropriée avec un message explicite
et ajoutez le test au constructeur :
`Passager(Position prise_en_charge, Position destination)`

Récupérez ce cas d'erreur avec les instructions `try .. catch`
dans la méthode `reset()` de la classe `Demo`

## Lecture d'une flotte de taxis (1 point)

À la classe `TaxiCompany` ajoutez une méthode `void lireTaxi(String filename)`,
qui remplira l'attribut `vehicules` avec les taxis lus
dans un fichier texte. Le fichier texte contiendra une ligne
par taxi contenant trois champs : nom du chauffeur, abscisse x, ordonnée y.
Exemple de fichier fourni `french_cab_company.txt` :

```
Lucienne 10 35
Marcel 10 35
Alfred 15 22
Rita 0 5
```

Les noms des chauffeurs de taxi ne seront pas utilisés dans le programme.

Les classes suivantes de la bibliothèque Java pourraient vous être utiles :
`FileReader`, `BufferedReader`, `Scanner` et `String`. Vous pouvez
vous inspirer en partie de la méthode `addEntriesFromFile` du projet
`adress-book`.

N'oubliez pas de gérer les exceptions dues aux problèmes de lecture
de fichier : fichier inexistant, fichier non conforme.

## Amélioration de la démo (3 points)

Actuellement la classe `Demo` ne prend en compte qu'un seul taxi avec
un seul passager. Votre objectif maintenant va consister à faire
fonctionner une simulation avec plusieurs taxis et plusieurs passagers.
Vous êtes libre de faire comme vous voulez, mais je vous propose le plan
suivant :

1. Modifiez la méthode `Passager creePassager()` de `PassagerSource`
   pour générer des positions aléatoires.
1. Implémentez l'interface `Actor` dans `PassagerSource` afin de faire
   des demandes de nouvelles courses à une fréquence de 10%.
1. Modifiez la méthode `reset()` de `Demo` pour lire une liste de taxis
   dans un fichier, ajoutez tous les véhicules aux `acteurs`
1. Dans la méthode `reset()` de `Demo` ajoutez un objet `PassagerSource`
   aux `acteurs`.
1. Dans la méthode `reset()` de `Demo`, supprimez ce qui concerne l'objet
`passager`.

Testez votre programme.

N'oubliez pas de *committer* votre travail après chaque modification avec un message
explicite.

À la fin de cette partie, même si vous n'avez pas tout fait et y reviendrez plus
tard, synchronisez votre code sur Bitbucket : `git push --all`

- - -

# Troisième partie : Ajout d'un véhicule de type Navette (6 points)

La compagnie de taxi gère également des navettes, une navette prend en charge
des individus à plusieurs emplacements et les emmènent à leur différentes
destinations. Une navette a un nombre maximum de places.

Vous ajouterez au programme de simulation une classe `Navette` similaire à `Taxi`.
Vous êtes libre de choisir l'algorithme de déplacement de la navette,
**faites simple** !!!

Vous ajouterez une seule navette à la simulation.

Vous ajouterez une classe  de test JUnit `NavetteTest` pour tester la classe
`Navette`.

N'oubliez pas de *committer* votre travail après chaque modification avec un message
explicite.

Ce travail nécessite de l'analyse et de la réflexion,
afin de ne pas rendre un programme vierge, il est conseillé
de travailler de manière incrémentale, comme nous l'avons
vu en cours, en commençant par les éléments les plus simples
à mettre en oeuvre. Voici un plan que je vous propose de suivre :

1. Ajoutez la classe `Navette.java` à la simulation, avec le minimum
   d'instructions pour que le programme compile.
1. *Committez cette classe* : `git add Navette.java` puis
   `git commit -m "Ajout de la classe Navette.java" Navette.java`
1. Ajoutez une navette à la simulation dans `Demo`, testez et *committez*.
1. Réfléchissez aux attributs spécifiques de la classe `Navette`
1. Intégrez ces attributs à la classe `Navette` puis *committez* après
   compilation.
1. Ajout une classe de test `NavetteTest` et un test de création
   correcte d'une navette puis *committez* après compilation et test.
1. Concevez l'algorithme de déplacement de la navette.
1. Ajoutez l'algorithme dans le commentaire *Javadoc* de la
   méthode `act()` de la classe `Navette` puis *committez*.
1. Ajoutez des tests pour tester le comportement attendu de `act()`
   puis *committez*
1. Programmez `act()` jusqu'à réussir à passer les tests et *committez*.

- - -

# Épilogue : enregistrement final (1 point)

Synchronisez votre travail avec le serveur Bitbucket par la commande :

```
git push --all
```


- - -