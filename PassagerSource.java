/**
 * Simule l'arrivée de passagers demandant des courses 
 * à une compagnie de taxi.
 * Des passagers devraient apparaître à des intervalles aléatoires.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
import java.util.*;

public /*abstract*/ class PassagerSource 
//implements Actor
{
    private TaxiCompany company;

    /**
     * Constructor for objects of class PassengerSource.
     * Constructeur des objets de la classe PassagerSource.
     * @param company La compagnie utilisée. Ne doit pas être null.
     * @throws NullPointerException si company est null.
     */
    public PassagerSource(TaxiCompany company)
    {
        if(company == null) {
            throw new NullPointerException("company");
        }
        this.company = company;
    }

    /**
     * Fait que la source engendre un nouveau passager et
     * demande une course à la compagnie.
     * @return true si la demande réussit, false autrement.
     */
    public boolean demandeCourse()
    {
        // **** CODE A MODIFIER POUR LE DS ****
        boolean res;
        //Passager linux = new Passager(new Position(5, 10), new Position(10, 20));
        
       Passager passager = creePassager();
       
     
       TaxiCompany company = new TaxiCompany();
       company.demandeCourse(passager);
        if (company.demandeCourse(passager) == true) {
            res = true;
           } 
        else {
               res = false;
            }
            return res;
          
      }
   

    /**
     * Crée un nouveau passager.
     * @return Le passager créé.
     */
    public Passager creePassager()
    {
        Random rand = new Random();
        int nombreAleatoire1 = rand.nextInt(50 - 0 + 1) + 0;
        int nombreAleatoire2 = rand.nextInt(50 - 0 + 1) + 0;
        int nombreAleatoire3 = rand.nextInt(50 - 0 + 1) + 0;
        int nombreAleatoire4 = rand.nextInt(50 - 0 + 1) + 0;
        return new Passager(new Position(nombreAleatoire1, nombreAleatoire2), new Position(nombreAleatoire3,nombreAleatoire4));
    }
}
