
/**
 * Write a description of class Navette here.
 * 
 * @author (Chloe) 
 * @version (version definitive)
 */
public class Navette 
{
    // instance variables - replace the example below with your own
    // Compagnie d'appartenance du véhcule
    private TaxiCompany company;
    // Où le véhicule se trouve.
    private Position position;
    // Où le véhicule se dirige.
    private Position destinationPosition;
    
    private Passager passager;
    
    private int nbPassager;
    /**
     * Constructor for objects of class Navette
     */
    public Navette(TaxiCompany comp, Position pos, Position destPos, int nbPass)
    {
        if(company != null) {
            this.company = comp;
        }
        else {
            throw new NullPointerException();
        }
        
        
        if(position != null) {
            this.position = pos;
        }
        else {
            throw new NullPointerException();
        }
        
       
        
        if(destinationPosition != null) {
            this.destinationPosition = destPos;
        }
        else {
            throw new NullPointerException();
        }
        
        if(nbPassager >= 0) {
            this.nbPassager = nbPass;
        }
        else {
            throw new IllegalArgumentException();
        }
    }
    
    /**
     * Prend en charge un passager.
     * Enregistre un passager de plus.
     * @param passager Le passager.
     */
    public void prendEnCharge(Passager passager)
    {
        this.nbPassager = nbPassager + 1;
    }

    /**
     * Fait descendre un passager.
     * enregistre un passager de moins
     */
    public void faitDescendrePassager()
    {
        nbPassager = nbPassager - 1;
        
    }
    
   
}
