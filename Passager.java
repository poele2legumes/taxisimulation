/**
 * Modélise un passager souhaitant aller d'une
 * position à une autre.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class Passager
{
    private Position prise_en_charge;
    private Position destination;

    /**
     * Constructeur des objets de la classe Passager
     * @param pickup La position de prise en charge, ne doit pas être null.
     * @param destination La position de destination, ne doit pas être null.
     * @throws NullPointerException Si une des positions est null.
     */
    public Passager(Position prise_en_charge, Position destination)
    {
        // **** CODE A ECRIRE ICI POUR LE DS ****
        
        if(prise_en_charge != destination) {
        
            if(prise_en_charge != null) {
                this.prise_en_charge = prise_en_charge;
            }
            else {
                throw new NullPointerException();
            }
            
            if(destination != null) {
                this.destination = destination;
            }
            else {
                throw new NullPointerException();
            }
            
       }else {
           throw new ArithmeticException();
       }
    }
    
    public String toString()
    {
        return " passager voyageant de " +
               prise_en_charge + " à " + destination;
    }

    /**
     * @return La position de prise en charge.
     */
    public Position getPriseEnChargePosition()
    {
        return prise_en_charge;
    }
    
    /**
     * @return La position de destination.
     */
    public Position getDestination()
    {
        return destination;
    }
}
