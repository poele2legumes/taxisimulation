import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

/**
 * Un unique passager est créé, et une prise en charge est demandée.
 * Quand la simulation tourne, le client devrait être
 * pris en charge puis déposé à sa destination.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class Demo
{
    private List<Actor> acteurs;

    /**
     * Constructeur pour les objets de la classe Demo
     */
    public Demo()
    {
        acteurs = new LinkedList<Actor>();
        reset();
    }
    
    /**
     * Exécute la démo pour un nombre fixe de pas.
     */
    public void run()
    {
        for(int step = 0; step < 100; step++) {
            step();
        }
    }

    /**
     * Exécute la démo pour un pas en demandant
     * à tous les acteurs d'agir.
     */
    public void step()
    {
        for(Actor acteur : acteurs) {
            acteur.act();
        }
    }
    
    /**
     * Réinitialise la démo au point de départ.
     * Un unique taxi est créé, et une prise en charge est
     * demandée pour un unique passager.
     * @throws IllegalStateException Si une prise en charge ne peut être trouvée
     */
    public void reset()
    {
        acteurs.clear();
        TaxiCompany company = new TaxiCompany();
        Taxi taxi = new Taxi(company, new Position(10, 10));
        List<Vehicule> vehicules = company.getVehicules();
        vehicules.add(taxi);
        acteurs.addAll(vehicules);
        /*
         * tentative de mise en place de l'ajout de PassagerSource à Actor
        Actor passagerSource = new PassagerSource();
        acteurs.addAll(passagerSource);*/
        
        Passager passager = new Passager(new Position(0, 0),
                                       new Position(10, 20));
        if(!company.demandeCourse(passager)) {
            throw new IllegalStateException("Impossible de fournir une course.");
        }
        
         try {
             Passager passagerException = new Passager(new Position(10, 10), new Position(10, 10));
            } 
            catch (ArithmeticException e) {
                System.out.println("Point de depart est egale au point d'arrive");
            }
    }
}
