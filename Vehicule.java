/**
 * Modélise les éléments communs aux différents véhicules.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public abstract class Vehicule implements Actor
{
    // Compagnie d'appartenance du véhcule
    private TaxiCompany company;
    // Où le véhicule se trouve.
    private Position position;
    // Où le véhicule se dirige.
    private Position destinationPosition;
    // Enregistre le temps où le véhicule ne fait rien.
    private int inactiviteCompteur;
    
    /**
     * Constructeur de la classe Véhicule
     * @param company La compagnie de taxi. Ne doit pas être null.
     * @param Position Le point de départ du véhicule. Ne doit pas être null.
     * @throws NullPointerException Si company ou position est null.
     */
    public Vehicule(TaxiCompany company, Position position)
    {
       // **** CODE A ECRIRE ICI POUR LE DS ****	
    	if(company != null) {
            this.company = company;
        }
        else {
            throw new NullPointerException();
        }
    	
    	if(position != null) {
            this.position = position;
        }
        else {
            throw new NullPointerException();
        }
    }
    
    
    /**
     * Avertit la compagnie de notre arrivée à la position de prise en charge.
     */
    public void avertiPriseEnChargeArrivee()
    {
        company.arriveAPriseEnCharge(this);
    }
    
    /**

     * Avertit la compagnie de notre arrivée à la destination du passager.
     */
    public void avertiPassagerArrivee(Passager passager)
    {
        company.arriveADestination(this, passager);
    }
    
    /**
     * Reçoit une position de prise en charge.
     * Comment ceci est traité dépend du type de véhicule.
     * @param Position La position de prise en charge.
     */
    public abstract void setPriseEnChargePosition(Position Position);
    
    /**
     * Charge un passager.
     * Comment ceci est traité dépend du type de véhicule.
     * @param passenger The passenger.
     */
    public abstract void prendEnCharge(Passager passager);
    
    /**
     * Est-ce que le véhicule est libre ?
     * @return Si oui ou non cevéhicule est libre.
     */
    public abstract boolean estLibre();
    
    /**
     * Fait descendre les passagers dont la destination est la
     * position courante.
     */
    public abstract void faitDescendrePassager();
    
    /**
     * Obtient la position.
     * @return Où le véhicule est actuellement positionné.
     */
    public Position getPosition()
    {
        return position;
    }
    
    /**
     * Établit la position courante.
     * @param Position Où c'est. Ne doit pas être null.
     * @throws NullPointerException Si position est null.
     */
    public void setPosition(Position position)
    {
        if(position != null) {
            this.position = position;
        }
        else {
            throw new NullPointerException();
        }
    }
    
    /**
     * Obtient la position de destination.
     * @return Vers où ce véhicule se dirige ou null si il est inactif.
     */
    public Position getDestinationPosition()
    {
        return destinationPosition;
    }
    
    /**
     * Établit la position de de destination demandée.
     * @param Position Où aller. Ne doit pas être null.
     * @throws NullPointerException Si position est null.
     */
    public void setDestinationPosition(Position Position)
    {
        if(Position != null) {
            destinationPosition = Position;
        }
        else {
            throw new NullPointerException();
        }
    }
    
    /**
     * Efface la position de destination.
     */
    public void effaceDestinationPosition()
    {
        destinationPosition = null;
    }

    /**
     * @return Combien de temps ce véhicule est resté inactif.
     */
    public int getInactiviteCompteur()
    {
        return inactiviteCompteur;
    }
    
    /**
     * Augmente le compteur d'inactivité de ce véhicule.
     */
    public void incrementInactiviteCompteur()
    {
        inactiviteCompteur++;
    }
}
